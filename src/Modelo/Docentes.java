/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author bmfil
 */
public class Docentes {
    private int numDocente;
    private String nombre;
    private String domicilio;
    private int nivel;
    private float pagoHora;
    private float horasImp;

    public Docentes(int numDocente, String nombre, String domicilio, int nivel, float pagoHora, float horasImp) {
        this.numDocente = numDocente;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.nivel = nivel;
        this.pagoHora = pagoHora;
        this.horasImp = horasImp;
    }
    public Docentes(){
        this.numDocente = 0;
        this.nombre = null;
        this.domicilio = null;
        this.nivel = 0;
        this.pagoHora = 0.0f;
        this.horasImp = 0.0f;
    }
    public Docentes(Docentes doc){
        this.numDocente = doc.numDocente;
        this.nombre =  doc.nombre;
        this.domicilio = doc.domicilio;
        this.nivel = doc.nivel;
        this.pagoHora = doc.pagoHora;
        this.horasImp = doc.horasImp;
    }

    public int getNumDocente() {
        return numDocente;
    }

    public void setNumDocente(int numDocente) {
        this.numDocente = numDocente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public float getPagoHora() {
        return pagoHora;
    }

    public void setPagoHora(float pagoHora) {
        this.pagoHora = pagoHora;
    }

    public float getHorasImp() {
        return horasImp;
    }

    public void setHorasImp(float horasImp) {
        this.horasImp = horasImp;
    }
   public float calcularPago(){
       float incre=0.0f;
       switch(nivel){
           case 0:
               incre = this.pagoHora + (this.pagoHora * 0.30f);
               break;
            case 1:
                incre = this.pagoHora + (this.pagoHora * 0.50f);
                break;
            case 2:
                incre = this.pagoHora;
                break;
       } 
       return incre * horasImp;
   }
   public float calcularImpuesto(){
       return this.calcularPago()*0.16f;
       
   }
   public float calcularBono(int cantHijos){
       float calculo = 0.0f;
       if(cantHijos >=1 && cantHijos <= 2){
           calculo = this.calcularPago() * 0.05f;
       }
       else if(cantHijos >= 3 && cantHijos <=5 ){
           calculo = this.calcularPago() * 0.10f;
       }else if (cantHijos >5 ){
           calculo = this.calcularPago() * 0.20f;
       }
       return calculo;
   }
}
