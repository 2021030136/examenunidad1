/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Controlador;

import Modelo.Docentes;
import Vista.jdldocente;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class controlador implements ActionListener {

    private Docentes doc;
    private jdldocente vista;

    public controlador(Docentes doc, jdldocente vista) {
        this.doc = doc;
        this.vista = vista;

        // se inicializan los botonos
        vista.btnguardar.addActionListener(this);
        vista.btnnuevo.addActionListener(this);
        vista.btnlimpiar.addActionListener(this);
        vista.btnmostrar.addActionListener(this);
        vista.btncancelar.addActionListener(this);
        vista.btncerrar.addActionListener(this);
    }
    //iniciamos vista 

    private void iniciarvista() {
        vista.setTitle("Pagos");
        vista.setSize(600, 600);
        vista.setVisible(true);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //btn nuevo
        if (e.getSource() == vista.btnnuevo) {
            vista.TxtpagoHora.setEnabled(true);
            vista.btnguardar.setEnabled(true);
            vista.cbxNivel.setEnabled(true);
            vista.spicantHijos.setEnabled(true);
            vista.txtNom.setEnabled(true);
            vista.btncancelar.setEnabled(true);
            vista.txtdom.setEnabled(true);
            vista.txthorasImp.setEnabled(true);
            vista.btnlimpiar.setEnabled(true);
            vista.btnmostrar.setEnabled(true);
            vista.txtnumDoc.setEnabled(true);
        }
        //btn cerrar
        if (e.getSource() == vista.btncerrar) {
            int respuesta = JOptionPane.showConfirmDialog(vista, "¿Quieres salir",
                    "Salir", JOptionPane.YES_NO_OPTION);
            if (respuesta == JOptionPane.YES_NO_OPTION) {
                vista.dispose();
                System.exit(0);
            }
        }
        if (e.getSource() == vista.btnlimpiar) {
            vista.TxtpagoHora.setText("");
            vista.txtNom.setText("");
            vista.txtdom.setText("");
            vista.txthorasImp.setText("");
            vista.txtnumDoc.setText("");
            vista.jlbPagoBono.setText("");
            vista.jlbdesImp.setText("");
            vista.jlbtot.setText("");
            vista.jlbpagoHora.setText("");

        }
        if (e.getSource() == vista.btncancelar) {
            vista.TxtpagoHora.setText("");
            vista.txtNom.setText("");
            vista.txtdom.setText("");
            vista.txthorasImp.setText("");
            vista.txtnumDoc.setText("");
            vista.jlbPagoBono.setText("");
            vista.jlbdesImp.setText("");
            vista.jlbtot.setText("");
            vista.jlbpagoHora.setText("");
            vista.TxtpagoHora.setEnabled(false);
            vista.btnguardar.setEnabled(false);
            vista.cbxNivel.setEnabled(false);
            vista.spicantHijos.setEnabled(false);
            vista.txtNom.setEnabled(false);
            vista.btncancelar.setEnabled(false);
            vista.txtdom.setEnabled(false);
            vista.txthorasImp.setEnabled(false);
            vista.btnlimpiar.setEnabled(false);
            vista.btnmostrar.setEnabled(false);
            vista.txtnumDoc.setEnabled(false);
        }
        if (e.getSource() == vista.btnguardar) {
            try{
            doc.setDomicilio(vista.txtdom.getText());
            doc.setNombre(vista.txtNom.getText());
            doc.setNivel(vista.cbxNivel.getSelectedIndex());
            doc.setNumDocente(Integer.parseInt(vista.txtnumDoc.getText()));
            doc.setHorasImp(Float.parseFloat(vista.txthorasImp.getText()));
            doc.setPagoHora(Float.parseFloat(vista.TxtpagoHora.getText())); 
            JOptionPane.showMessageDialog(vista, "Guardado con exito");
            
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(vista, "Surgio el siguiente error" + ex.getMessage());
            } catch (Exception ex2) {
                JOptionPane.showMessageDialog(vista, "Surgio el siguiente error" + ex2.getMessage());
            }
            
            //doc.setNivel(Integer.parseInt(vista.spicantHijos.getValue().toString()));

            
        }
        if (e.getSource() == vista.btnmostrar) {
            float x, y, z;
            x = doc.calcularImpuesto();
            y = doc.calcularBono((int) vista.spicantHijos.getValue());
            z = doc.calcularPago();
            vista.txtdom.setText(String.valueOf(doc.getDomicilio()));
            vista.txtnumDoc.setText(String.valueOf(doc.getNumDocente()));
            vista.txtNom.setText(String.valueOf(doc.getNombre()));
            vista.txthorasImp.setText(String.valueOf(doc.getHorasImp()));
            vista.TxtpagoHora.setText(String.valueOf(doc.getPagoHora()));
            vista.jlbPagoBono.setText(String.valueOf(doc.calcularBono(Integer.parseInt(vista.spicantHijos.getValue().toString()))));
            vista.jlbdesImp.setText(String.valueOf(doc.calcularImpuesto()));
            vista.jlbpagoHora.setText(String.valueOf(doc.calcularPago()));
            vista.jlbtot.setText(String.valueOf(z - x + y));
        }
    }

    public static void main(String[] args) {
        // TODO code application logic here

        Docentes doce = new Docentes();
        jdldocente vista = new jdldocente(new JFrame(), true);
        controlador contra = new controlador(doce, vista);
        contra.iniciarvista();
    }

}
