/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package Vista;

/**
 *
 * @author bmfil
 */
public class jdldocente extends javax.swing.JDialog {

    /**
     * Creates new form jdldocente
     */
    public jdldocente(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        spicantHijos = new javax.swing.JSpinner();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jlbpagoHora = new javax.swing.JLabel();
        jlbPagoBono = new javax.swing.JLabel();
        jlbdesImp = new javax.swing.JLabel();
        jlbtot = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtnumDoc = new javax.swing.JTextField();
        txtNom = new javax.swing.JTextField();
        txtdom = new javax.swing.JTextField();
        TxtpagoHora = new javax.swing.JTextField();
        txthorasImp = new javax.swing.JTextField();
        cbxNivel = new javax.swing.JComboBox<>();
        jPanel2 = new javax.swing.JPanel();
        btnnuevo = new javax.swing.JButton();
        btnguardar = new javax.swing.JButton();
        btnmostrar = new javax.swing.JButton();
        btnlimpiar = new javax.swing.JButton();
        btncancelar = new javax.swing.JButton();
        btncerrar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Calculo de Pago\n", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Segoe UI", 0, 14))); // NOI18N
        jPanel1.setLayout(null);

        jLabel7.setText("Pago por hora impartida");
        jPanel1.add(jLabel7);
        jLabel7.setBounds(20, 30, 150, 16);

        jLabel8.setText("Hijos");
        jPanel1.add(jLabel8);
        jLabel8.setBounds(20, 70, 50, 16);

        spicantHijos.setOpaque(false);
        jPanel1.add(spicantHijos);
        spicantHijos.setBounds(70, 70, 30, 22);

        jLabel9.setText("Pago por Bono");
        jPanel1.add(jLabel9);
        jLabel9.setBounds(110, 70, 90, 16);

        jLabel10.setText("Descuento por Impuesto");
        jPanel1.add(jLabel10);
        jLabel10.setBounds(20, 100, 170, 16);

        jLabel11.setText("Total a pagar ");
        jPanel1.add(jLabel11);
        jLabel11.setBounds(20, 130, 100, 16);
        jPanel1.add(jlbpagoHora);
        jlbpagoHora.setBounds(230, 30, 90, 20);
        jPanel1.add(jlbPagoBono);
        jlbPagoBono.setBounds(230, 70, 50, 20);
        jPanel1.add(jlbdesImp);
        jlbdesImp.setBounds(240, 100, 70, 16);
        jPanel1.add(jlbtot);
        jlbtot.setBounds(170, 130, 70, 10);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(20, 250, 380, 150);

        jLabel1.setText("num. Docente");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(30, 50, 90, 16);

        jLabel2.setText("Nombre");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(30, 80, 80, 16);

        jLabel3.setText("Domicilio");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(30, 120, 80, 16);

        jLabel4.setText("Nivel");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(30, 150, 80, 16);

        jLabel5.setText("Pago por hora base");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(20, 170, 110, 16);

        jLabel6.setText("Horas Impartidas");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(20, 200, 100, 16);

        txtnumDoc.setEnabled(false);
        getContentPane().add(txtnumDoc);
        txtnumDoc.setBounds(140, 50, 63, 22);

        txtNom.setEnabled(false);
        getContentPane().add(txtNom);
        txtNom.setBounds(140, 80, 140, 22);

        txtdom.setEnabled(false);
        getContentPane().add(txtdom);
        txtdom.setBounds(140, 110, 180, 22);

        TxtpagoHora.setEnabled(false);
        getContentPane().add(TxtpagoHora);
        TxtpagoHora.setBounds(140, 170, 80, 22);

        txthorasImp.setEnabled(false);
        getContentPane().add(txthorasImp);
        txthorasImp.setBounds(140, 200, 80, 22);

        cbxNivel.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Licenciatura o Ingieneria", "Maestro en Ciencias ", "Doctor " }));
        getContentPane().add(cbxNivel);
        cbxNivel.setBounds(140, 140, 160, 22);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Acciones", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 0, 14))); // NOI18N

        btnnuevo.setText("Nuevo");

        btnguardar.setText("Guardar");
        btnguardar.setEnabled(false);

        btnmostrar.setText("Mostrar");
        btnmostrar.setEnabled(false);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnnuevo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnguardar, javax.swing.GroupLayout.DEFAULT_SIZE, 86, Short.MAX_VALUE)
                    .addComponent(btnmostrar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnnuevo, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnguardar, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnmostrar, javax.swing.GroupLayout.DEFAULT_SIZE, 51, Short.MAX_VALUE)
                .addContainerGap())
        );

        getContentPane().add(jPanel2);
        jPanel2.setBounds(420, 20, 120, 250);

        btnlimpiar.setText("Limpiar");
        getContentPane().add(btnlimpiar);
        btnlimpiar.setBounds(30, 430, 90, 40);

        btncancelar.setText("Cancelar ");
        getContentPane().add(btncancelar);
        btncancelar.setBounds(140, 430, 100, 40);

        btncerrar.setText("Cerrar");
        getContentPane().add(btncerrar);
        btncerrar.setBounds(270, 430, 90, 40);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(jdldocente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(jdldocente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(jdldocente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(jdldocente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                jdldocente dialog = new jdldocente(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JTextField TxtpagoHora;
    public javax.swing.JButton btncancelar;
    public javax.swing.JButton btncerrar;
    public javax.swing.JButton btnguardar;
    public javax.swing.JButton btnlimpiar;
    public javax.swing.JButton btnmostrar;
    public javax.swing.JButton btnnuevo;
    public javax.swing.JComboBox<String> cbxNivel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    public javax.swing.JLabel jlbPagoBono;
    public javax.swing.JLabel jlbdesImp;
    public javax.swing.JLabel jlbpagoHora;
    public javax.swing.JLabel jlbtot;
    public javax.swing.JSpinner spicantHijos;
    public javax.swing.JTextField txtNom;
    public javax.swing.JTextField txtdom;
    public javax.swing.JTextField txthorasImp;
    public javax.swing.JTextField txtnumDoc;
    // End of variables declaration//GEN-END:variables
}
